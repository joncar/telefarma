services.factory('pushGcm',function($rootScope,UI,$ionicLoading,$ionicPlatform,Api,$http,$ionicPopup){
    return {
            init:function($scope){
                if(typeof(window.PushNotification)!=='undefined'){
                    //$scope.loading = UI.getLoadingBox($ionicLoading,'Registrando ID de app');
                    var push = window.PushNotification.init({
                        android: {
                            senderID: "826153943994"
                        },
                        ios: {
                            alert: "true",
                            badge: true,
                            sound: 'false'
                        },
                        windows: {}
                    });

                    push.on('registration', function(data) {
                         if(localStorage.gcm!==data.registrationId){
                            $scope.data = {repartidores_id:$scope.user,valor:data.registrationId,tipo:ionic.Platform.isIOS()?'IOS':'Android'};
                            Api.insert('gcm_repartidores',$scope,$http,function(data){                                
                                localStorage.gcm = $scope.data.gcm;
                            });
                        }
                    });

                    push.on('notification', function(data) {
                        if(typeof($scope.alertPopup)==='undefined'){
                            $scope.showAlert = UI.getShowAlert($ionicPopup);
                            $scope.showAlert(data.title,data.message);
                        }
                    });

                    push.on('error', function(e) {
                        alert( e.message);
                    });
                }
            }
    };
});