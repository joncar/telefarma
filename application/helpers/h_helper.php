<?php 
	function  correo($email = 'joncar.c@gmail.com',$titulo = '',$msj = '',$from='joncar.info<info@joncar.info>')
        {            
            $sfrom=$from; //cuenta que envia
            $sdestinatario=$email; //cuenta destino
            $ssubject=$titulo; //subject
            $shtml=$msj; //mensaje
            $shtml.='<b>Nota: Hemos quitado los acentos de este mail por motivos de visualizacion</b>';
            $sheader="From:".$sfrom."\nReply-To:".$sfrom."\n";
            $sheader=$sheader."X-Mailer:PHP/".phpversion()."\n";
            $sheader=$sheader."Mime-Version: 1.0\n";
            $sheader=$sheader."Content-Type: text/html; charset:utf-8";
            mail($sdestinatario,$ssubject,$shtml,$sheader);
        }
	
	
	function mail_activate($email,$name,$codigo_activacion)
	{
		$template = template_newusr_mail($name,$email,$codigo_activacion);
		correo($email,'Hola, '.$name.' gracias por registrarte, activa tu cuenta',$template);
	}
	
	function mail_recover($email,$codigo)
	{
		$template = template_recover_mail($email,$codigo);
		correo($email,'Solicitud de restablecimiento de contraseña',$template);
	}
	
	/********* templates mail ***********/
	function template_newusr_mail($name,$email,$codigo)
        {
	$ruta = base_url('registrar/activate/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Estimado '.$name.' gracias por registrarte en halfandhalf.es</h1>
	<h3>Puedes activar tu cuenta para poder publicar anuncios.</h3>
	<b>Pulsa en el enlace para activar tu cuenta <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	<p align="center"><a href="#">Politicas y condiciones de uso</a> | <a href="#">Contactenos</a></p>
	</div>
</div>';
        }
		
		
	function template_recover_mail($email,$codigo)
    {
	$ruta = base_url('inicio/recover/'.urlencode($email).'/'.$codigo);
	return '<div style="background:#77b7c8; color:#004455; width:600px; height:auto; border-radius:1em; -moz-border-radius:1em; border:1px solid #004455; padding:10px;">
	<div style="background:white; border-radius:1em; -moz-border-radius:1em; padding:10px;">
	<h1 style="text-align:center">Haz solicitado tu contraseña</h1>
	<b>Pulsa en el enlace para restablecer tu contraseña <br/><a href="'.$ruta.'">'.$ruta.'</a></b>
	</div>
</div>';
    }
    
    function paypal_button($product,$price)
    {
        return '
        <form name="_xclick" action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" value="joncar.c@gmail.com">
        <input type="hidden" name="currency_code" value="USD">
        <input type="hidden" name="item_name" value="'.$product.'">
        <input type="hidden" name="amount" value="0">
        <button type="submit"><img src="'.base_url('img/paypal.jpg').'" style="width:100%; border-radius:1em"></button>
        </form>';               
    }
    
    function stringtosql($sentencia)
        {
            $se = explode("from",$sentencia);
            $se[0] = str_replace("select","",$se[0]);
            if(strpos($sentencia,'where')!==false){
            $s = explode("where",$se[1]);
            $se[1] = $s[0];
            $se[2] = $s[1];
            }
            return $se;
        }
        
        function sqltodata($query)
        {
            $data = '';            
            if($query->num_rows()>0){
                foreach($query->result() as $s){
                    foreach($s as $x)
                        $data.= $x==null?'NULL':$x.' ';
                }                 
            }            
            return $data;
        }
        
        function sqltotable($query)
        {
            $data = '';                
            if($query->num_rows()>0){
                foreach($query->result() as $n=>$s){                    
                    $data.='<tr>';
                    foreach($s as $x)
                        $data.='<td>'.$x.'</td>';
                    $data.='</tr>';
                }    
                $h='<tr>';
                    foreach($query->result_array() as $n=>$r){
                        if($n==0){
                        foreach($r as $p=>$z)
                        $h.='<td><b>'.$p.'</b></td>';
                        }
                    }
                $h.='</tr>';                
                return '<table border="1">'.$h.$data.'</table>';
            }                        
            else return 'Sin datos para mostrar';
        }
    
    function fragmentar($TheStr, $sLeft, $sRight){             
        $d = array();
        foreach(explode($sLeft,$TheStr) as $n=>$c){
            if($n>0)
            array_push($d,substr($c,0,strpos($c,$sRight,0)));
        }                
        return $d; 
    }   
    
    function distribucion_list($compra,$texto){
        $str = '';
        //get_instance()->db->select('distribucion.*, sucursales.denominacion as sucursal_nombre');
        //get_instance()->db->join('sucursales','sucursales.id = distribucion.sucursal');
        $distribucion = get_instance()->db->get_where('sucursales');
        if($distribucion->num_rows()==0)$str.= 'Sin datos para mostrar';
        else{            
            foreach($distribucion->result() as $n=>$d){                
                $str.= '<table style="width:100%; margin-bottom:20px; margin-top:20px; table-layout:fixed" cellspacing="0">';
                if($n==0){
                $str.= '<tr>';
                    $str.= '<td><b>Código</b></td>
                            <td><b>Nombre</b></td>
                            <td><b>Lote</b></td>
                            <td><b>Vencimiento</b></td>
                            <td style="width:15%"><b>Cantidad</b></td>';
                $str.= '</tr>';
                }
                $str.= '<tr><td colspan="5"><b>'.$d->denominacion.'</b></td></tr>';
                get_instance()->db->select('distribucion.producto,productos.nombre_comercial,distribucion.lote,distribucion.vencimiento,distribucion.cantidad');
                get_instance()->db->join('productos','productos.codigo = distribucion.producto');
                $productos = get_instance()->db->get_where('distribucion',array('compra'=>$compra,'sucursal'=>$d->id));
                foreach($productos->result() as $p){
                    $lote = empty($p->lote) || $p->lote==0?'N/A':$p->lote;
                    $str.= '<tr>';
                        $str.= '<td style="width:20%">'.$p->producto.'</td>
                                <td style="width:40%">'.$p->nombre_comercial.'</td>
                                <td style="width:10%">'.$lote.'</td>
                                <td style="width:15%">'.$p->vencimiento.'</td>
                                <td style="width:15%">'.$p->cantidad.'</td>';
                    $str.= '</tr>';
                }
                $str.= '</table>';
                $str.= '';
            }
            
        }
        $texto = str_replace('{function=distribucion_list}',$str,$texto);
        return $texto;
    }
    
    function cortar_palabras($str,$cant){
        $descr = explode(' ',$str);
        $str = '';
        $paso = false;
        foreach($descr as $n=>$d){
            if($n<$cant){
                $str.=$d.' ';
            }else{
                $paso = true;
            }
        }        
        if($paso){
            $str.= '...';
        }
        return $str;
    }
    
    function refresh_list(){
        return '<script>$(".flexigrid").find(".filtering_form").trigger("submit");</script>';
    }
    
    function getMenu($menu,$labels = array()){
        $str = '';
        foreach($menu as $n=>$m){
            if(!empty($m)){
                if(!empty($labels[$n])){
                    $label = $labels[$n][0];
                    $icon = !empty($labels[$n][1])?$labels[$n][1]:'fa fa-graduation-cap';
                }else{
                    $label = explode('/',$n);
                    $label = $label[count($label)-1];
                    $label = isset($labels[$n][0])?$labels[$n][0]:$label;
                    $label = ucfirst(str_replace('_',' ',$label));
                    $icon = 'fa fa-graduation-cap';
                }
                $str.= '<li>
                    <a class="dropdown-toggle" href="#">
                            <i class="menu-icon '.$icon.'"></i>
                            <span class="menu-text">'.$label.'</span>
                            <b class="arrow fa fa-angle-down"></b>
                    </a>
                    <b class="arrow"></b>
                    <ul class="submenu">';
                    foreach($m as $ff=>$f){
                        if(is_array($f)){
                            $str.= getMenu($f,$labels);
                        }else{
                            $label = explode('/',$f);
                            $label = $label[count($label)-1];
                            $label = isset($labels[$label][0])?$labels[$label][0]:$label;
                            $label = ucfirst(str_replace('_',' ',$label));
                            $str.= '<li><a href="'.base_url($n.'/'.$f).'">'.$label.'</a></li>';
                        }
                    }
                    $str.= '</ul>
                </li>';
            }
        }
        return $str;
    }

    function myException($exception)

        {

           echo get_instance()->load->view('template',array('view'=>'errors/404','msj'=> $exception->getMessage()),TRUE); 

        }

        set_exception_handler('myException');