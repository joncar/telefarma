<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Rep extends Panel{
        function __construct() {
            parent::__construct();
        }

        function verReportes($id = ''){
            $permited = array('PDF','HTML','EXCEL');
            if(count($this->uri->segments)>5){
                $vars = $this->uri->segments;
                $_POST = empty($_POST)?array():$_POST;
                $_POST['docType'] = empty($_POST['docType'])?$vars[5]:$_POST['docType'];
                for($i=6;$i<count($this->uri->segments);$i+=2){
                    $_POST[$vars[$i]] = $vars[$i+1];
                }                
            }
            $reporte = $this->db->get_where('rep',array('id'=>$id));
            if($reporte->num_rows()>0){
                $reporte = $reporte->row();
                //$this->mostrarReporte($reporte,array('proveedores_id'=>'2'));
                if(!empty($reporte->variables) && (empty($_POST) || (count($_POST)!=count(explode(',',$reporte->variables))+1))){
                    //Mostrar form
                    $this->mostrarForm($reporte);
                }
                elseif(empty($reporte->variables) || (!empty($reporte->variables) && !empty($_POST))){
                    //Convertir fechas
                    if(!empty($_POST['desde'])){
                        $_POST['desde'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['desde'])));
                    }
                    if(!empty($_POST['hasta'])){
                        $_POST['hasta'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$_POST['hasta'])));
                    }
                    $this->mostrarReporte($reporte,$_POST);
                }
            }
        }
        
        function reemplazarFunciones($reporte){
            //En letras
            if(strstr('ValorEnLetras',$reporte->contenido)){
                $this->load->library('enletras');
                foreach(fragmentar($reporte->contenido,'ValorEnLetras(',')') as $f){
                    list($val,$mon) = explode(',',$f);
                    $reporte->contenido = str_replace('ValorEnLetras('.$f.')',$this->enletras->ValorEnLetras($val,$mon),$reporte->contenido);
                }
            }
            
            if(strpos(strip_tags($reporte->contenido),'barcode')){  
                $this->load->library('barcode');
                foreach(fragmentar($reporte->contenido,'barcode(',')') as $f){
                    list($val,$siz) = explode(',',$f);
                    $data = new Barcode();
                    $data = $data->draw($val,40,$siz);
                    $reporte->contenido = str_replace('barcode('.$f.')',$data,$reporte->contenido);
                }
            }
            
            //Explode
            if(strpos(strip_tags($reporte->contenido),'explode')){                  
                foreach(fragmentar($reporte->contenido,'explode(',')') as $f){
                    list($val,$delimiter) = explode(',',$f);
                    $str = '';
                    foreach(explode($delimiter,$val) as $e){
                        $str.= $e.'<br/>';
                    }
                    $reporte->contenido = str_replace('explode('.$f.')',$str,$reporte->contenido);
                }
            }
            
            //Banner
            if(strpos(strip_tags($reporte->contenido),'date')){                  
                foreach(fragmentar($reporte->contenido,'date(',')') as $f){                    
                    $reporte->contenido = str_replace('{date('.$f.')}',date($f),$reporte->contenido);
                }
            }
            
            //Date
            if(strpos(strip_tags($reporte->contenido),'banner')){                  
                foreach(fragmentar($reporte->contenido,'banner(',')') as $f){
                    $qr = $this->db->get_where('facultades',array('id'=>$this->user->facultad));
                    $reporte->contenido = str_replace('{banner('.$f.')}','<img alt="" src="'.base_url('img/fotos_facultades/'.$qr->row()->banner).'" width="615" height="93">',$reporte->contenido);
                }
            }
            
            //grafico
            if(strpos(strip_tags($reporte->contenido),'Grafico')){                
                include(APPPATH.'/libraries/graph/Phpgraphlib.php');
                foreach(fragmentar($reporte->contenido,'Grafico(',')') as $f){
                    list($array,$width,$height,$title) = explode(',',$f);
                    $graph = new PHPGraphLib($width,$height,'/var/www/html/deporvida/img/grafico.png');                    
                    foreach($consultas[$array]->result() as $r){
                        $data = array();
                        foreach($r as $n=>$v){
                            $data[$n] = $v;
                        }
                         $graph->addData($data);
                    }                    
                   
                    $graph->setTitle($title);
                    $graph->setBars(false);
                    $graph->setLine(true);
                    $graph->setDataPoints(true);
                    $graph->setDataPointColor('maroon');
                    $graph->setDataValues(true);
                    $graph->setDataValueColor('maroon');
                    $graph->setGoalLine(.0025);
                    $graph->setGoalLineColor('red');
                    $graph->createGraph();
                    $reporte->contenido = str_replace('Grafico('.$f.')','<img src="'.base_url('img/grafico.png').'">',$reporte->contenido);                    
                }
            }
            return $reporte;
        }
        
        function mostrarForm($reporte){
            
            $variables = explode(',',$reporte->variables);            
            $this->loadView(array('view'=>'form','var'=>$variables,'reporte'=>$reporte));
        }
        
        function mostrarReporte($reporte,$variables){
            $reporte->query = strip_tags($reporte->query);
            $reporte->query = str_replace(chr(194),"",$reporte->query);
            $reporte->query = str_replace('&#39;','\'',$reporte->query);
            $reporte->query = str_replace('&gt;','>',$reporte->query);
            $reporte->query = str_replace('&lt;','<',$reporte->query);
            $reporte->query = str_replace('&nbsp;',' ',$reporte->query);
            $reporte->query = str_replace('Â','',$reporte->query);
            $reporte->query = utf8_decode($reporte->query);
            $reporte->query = str_replace('?','',$reporte->query);
            $querys = explode(';',$reporte->query);
            
            $consultas = array();
            //Sacamos el nombre de la variable y su query                        
            for($i=0;$i<count($querys);$i++){
                $posicion = strpos($querys[$i],"=");
                $variable = trim(substr($querys[$i],$i,$posicion));
                $variable = trim(str_replace('=','',$variable));
                $query = substr($querys[$i],$posicion);
                $query = substr($query,(strpos($query,'=')+1)); //Quitamos el otro igual                
                $query = str_replace('|selec|','SELECT',$query); //Quitamos la validación XSS
                foreach($variables as $n=>$v){
                    $query = str_replace('$_'.$n,"'".$v."'",$query);
                }
                //Reemplazamos User
                foreach($this->user as $n=>$v){
                    if(!is_array($v)){
                        $query = str_replace('$user_'.$n,"'".$v."'",$query);
                    }
                }
                //Consultamos
                $consultas[$variable] = $this->db->query($query);                
            }            
            //Reemplazamos variables globales
            $qr = $this->db->get_where('facultades',array('id'=>$this->user->facultad));
            $reporte->contenido = str_replace('[banner]','<img alt="" src="'.base_url('img/fotos_facultades/'.$qr->row()->banner).'" width="615" height="93">',$reporte->contenido);
            //Configuramos cuerpo
            $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');
            for($i = 0;$i<count($cuerpo);$i++){
                $data = explode(':',$cuerpo[$i]);
                $variable = $data[0];
                if(!empty($consultas[$variable])){
                    $qr=$consultas[$variable];                      
                    if(in_array('header',$data)){
                        $head = fragmentar($reporte->contenido,'['.$data[0].':header:'.$data[2].']','['.$data[0].':endheader]');                        
                        $body = fragmentar($reporte->contenido,'['.$data[0].':body]','['.$data[0].':endbody]');
                        $head[0] = str_replace('<p>','',$head[0]);
                        $head[0] = str_replace('</p>','',$head[0]);
                        $body[0] = str_replace('<p>','',$body[0]);
                        $body[0] = str_replace('</p>','',$body[0]);
                        
                        $head[1] = $head[0];
                        $body[1] = $body[0];
                        $cnt = '';
                        
                        for($i=0;$i<$qr->num_rows();$i++){
                            $cnt.= str_replace('_i_',$i,$body[0]);
                        }           
                        if($data[2]=='table'){
                            //Remplazar div por tablas                        
                            $head[0] = str_replace("<header>","<table border='1'><thead><tr>",$head[0]);
                            $head[0] = str_replace("<div","<th",$head[0]);
                            $head[0] = str_replace("</div>","</th>",$head[0]);
                            $head[0] = str_replace("</header>","</tr></thead>",$head[0]);
                            $body[0] = $cnt;
                            $body[0] = "<tbody>".str_replace("<section>","<tr>",$body[0]);                        
                            $body[0] = str_replace("<div ","<td ",$body[0]);
                            $body[0] = str_replace("</div>","</td>",$body[0]);
                            $body[0] = str_replace("</section>","</tr>",$body[0])."</tbody></table>";                        
                            $cnt = $head[0].$body[0];
                            $cnt = str_replace('display: inline-block;','',$cnt);
                        }
                        $reporte->contenido = str_replace('['.$data[0].':header:'.$data[2].']','',$reporte->contenido);
                        $reporte->contenido = str_replace('['.$data[0].':endheader]','',$reporte->contenido);
                        $reporte->contenido = str_replace($head[1],'',$reporte->contenido);
                        //Rempalzar body
                        $reporte->contenido = str_replace($body[1],$cnt,$reporte->contenido);
                        
                        $reporte->contenido = str_replace('['.$data[0].':body]','',$reporte->contenido);
                        $reporte->contenido = str_replace('['.$data[0].':endbody]','',$reporte->contenido);
                        
                        //Resetear for
                        $cuerpo = fragmentar(strip_tags($reporte->contenido),'[',']');
                        $i = -1;                        
                    }
                    
                    if(in_array('table',$data) && !empty($cuerpo[$i])){ //Tabla
                        $reporte->contenido = str_replace('['.$cuerpo[$i].']',sqltotable($qr,$data[2]),$reporte->contenido);
                    }                    
                    elseif(count($data)==3 && !in_array('_i_',$data) && !empty($cuerpo[$i])){ //Mostrar  
                        $da = !empty($qr->row($data[1])->{$data[2]})?$qr->row($data[1])->{$data[2]}:'No Encontrado';
                        $reporte->contenido = str_replace('['.$cuerpo[$i].']',$da,$reporte->contenido);
                    }
                }
            }
            
            //Reemplazamos muestreo de variables input
            foreach($variables as $n=>$v){
                $reporte->contenido = str_replace('$_'.$n,$v,$reporte->contenido);
            }
            //Calculamos las funciones llamadas desde el reporte
            $reporte = $this->reemplazarFunciones($reporte);
            $_POST['docType'] = empty($_POST['docType'])?'html':$_POST['docType'];
            switch($_POST['docType']){
                case 'pdf':
                    $this->load->library('html2pdf/html2pdf');
                    $papel = 'A4';
                    $orientacion = empty($reporte->contenido)?'P':$reporte->contenido;
                    $html2pdf = new HTML2PDF($orientacion,$papel,'es', false, 'ISO-8859-15', array(5,5,5,8));
                    $html2pdf->setDefaultFont('courier');
                    $html2pdf->writeHTML(utf8_decode($reporte->contenido));
                    ob_clean();
                    $html2pdf->Output('Reporte-'.date("dmY").'-'.$reporte->titulo.'.pdf');
                break;
                case 'html':
                    echo $reporte->contenido;
                break;
                case 'csv':
                    
                break;
            }            
            
        }
        

        function reportMaker(){
            $this->as['reportMaker'] = 'rep';
            $crud = $this->crud_function('','',$this);        
            if($crud->getParameters()=='add'){
                $crud->set_rules('identificador','Identificador','required|alpha_numeric|callback_identificador');
            }            
            $crud->field_type('orientacion','dropdown',array('P'=>'Vertical','L'=>'Horizontal'));
            $crud->set_rules('query','QUERY','required');
            $crud->add_action('<i class="fa fa-eye"></i> Ver Reporte','',base_url('reportes/rep/verReportes/').'/');
            //$crud->set_clone();
            $this->loadView($crud->render());
        }  
    }
?>
