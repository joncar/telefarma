$(function(){
    try{
    $('.datetime-input').datetimepicker({
    	timeFormat: 'hh:mm:ss',
		dateFormat: 'dd/mm/yy',
		showButtonPanel: true,
		changeMonth: true,
		changeYear: true
    });
    
	$('.datetime-input-clear').button();
	
	$('.datetime-input-clear').click(function(){
		$(this).parent().find('.datetime-input').val("");
		return false;
	});	
    }catch(e){
        
    }
});