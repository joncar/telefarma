<?php if(!empty($list)): ?>
<?php 
    $column_width = (int)(80/count($columns));    
?>
<div style="overflow: auto">
<table class="table table-bordered table-condensed table-striped">                        
    <thead>
            <tr>                    
                    <?php foreach($columns as $column){?>
                    <th style='cursor:pointer;' ng-click="change_order('<?= $column->field_name ?>')">                        
                        <?php echo $column->display_as?>
                        <span id="th_<?= $column->field_name ?>"></span>
                    </th>
                    <?php }?>
                    <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
                    <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                            <div class="text-right">
                                    <?php echo $this->l('list_actions'); ?>
                            </div>
                    </th>
                    <?php }?>
            </tr>
    </thead>		
    <tbody>
        <?php foreach($list as $num_row => $row){ ?>        
            <tr <?php if($num_row % 2 == 1){?>class="erow"<?php }?>>
                <?php foreach($columns as $column){?>
                <td width='<?php echo $column_width?>%' class='<?php if(isset($order_by[0]) &&  $column->field_name == $order_by[0]){?>sorted<?php }?>'>
                        <div class='text-left'><?php echo $row->{$column->field_name} != '' ? $row->{$column->field_name} : '&nbsp;' ; ?></div>
                </td>
                <?php }?>
                <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){ ?>
                <td align="left" width='20%'>
                    <div class='tools'>				                                                        
                        <?php if(!$unset_read){?>
                            <a href='<?php echo $row->read_url?>' title='<?php echo $this->l('list_view')?> <?php echo $subject?>' class="edit_button">
                                <i class="glyphicon glyphicon-read"></i>
                            </a>
                        <?php }?>
                        
                        <?php if(!$unset_edit){?>
                            <a href='<?php echo $row->edit_url?>' title='<?php echo $this->l('list_edit')?> <?php echo $subject?>' class="edit_button">
                                <i class="glyphicon glyphicon-edit"></i>
                            </a>
                        <?php }?>                        

                        <?php if(!$unset_delete){?>
                            <a href='<?php echo $row->delete_url?>' title='<?php echo $this->l('list_delete')?> <?php echo $subject?>' class="delete-row" >
                                <i class="glyphicon glyphicon-remove"></i>
                            </a>
                        <?php }?>                       

                        <?php 
                            if(!empty($row->action_urls)){
                            foreach($row->action_urls as $action_unique_id => $action_url){ 
                                    $action = $actions[$action_unique_id];
                        ?>
                            <a href="<?php echo $action_url; ?>" class="<?php echo $action->css_class; ?> crud-action"><?php 
                                ?><?= $action->label ?><?php 	
                            ?></a>		
                        <?php }
                        }
                        ?>
                    </div>
                </td>
                <?php } ?>
            </tr>
        <?php } ?>  
    </tbody>
</table>
</div>
<?php else: ?>
Sin datos para mostrar
<?php endif; ?>
